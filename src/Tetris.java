import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class Tetris extends JFrame {

    JLabel score; 


    public Tetris() {

        score = new JLabel(" 0"); // JLabel 객체 생성, 문자열 '0' 표시
        add(score, BorderLayout.SOUTH); // score 에 레이아웃 하단으로 배치 
        Board board = new Board(this);
        add(board); // board 추가
        board.start();

        setSize(400, 800); // Size 400x800
        setTitle("Tetris");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
   }

   public JLabel getscore() {
       return score;
   }

    public static void main(String[] args) {

        Tetris game = new Tetris();
        game.setLocationRelativeTo(null); //창 화면 중앙에 띄움
        game.setVisible(true); // 프레임 보이기 

    }
}
