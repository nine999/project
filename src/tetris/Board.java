
package Tetris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import Tetris.Shape.Tetrominoes;


public class Board extends JPanel implements ActionListener {
//보드를 그리는 클래스 패널에 추가할 것이기에 패널을 상속 받고 액션처리를 위해 리스너 인터페이스 구현

    final int BoardWidth = 20;	//보드의 넓이 항시 고정
    final int BoardHeight = 44;	//보드의 높이 항시 고정

    Timer t;	//시간 관련 변수 이를 통해서 화면에 표시
    boolean isFallingFinished = false;	//충돌 조건을 위한 변수
    boolean isStarted = false;	//시작되었음을 알리는 변수
    boolean isPaused = false;	//잠시 정지
    int numLinesRemoved = 0;
    int curX = 0;	//x좌표 초기화
    int curY = 0;	//y좌표 초기화
    JLabel score;	//label의 특성으로 점수를 표기
    JLabel gameover;
    Shape curPiece;	//현재 블록의 모양을 저장할 변수 shape 클래스. java 라이브러리 아님
    Tetrominoes[] board;	//보드판 생성
    boolean gamestatus;

 

    public Board(Tetris parent) {
    	//테트리스 클래스의 인자를 넘길 것. Boar의 생성자
       setFocusable(true);	//키보드 이벤트 러치를 위해
       curPiece = new Shape();	//현재 블록을 그리기 위해 shape 객체를 참조
       t = new Timer(400, this);	//떨어지는 속도 조절 시간이 된다면 수를 Scanner로 받아서 난이도 선택가능하게끔 구현
       t.start();	//시작!

       score =  parent.getscore();	//점수를 받아와서 저장 그리고 라벨에 표시
       board = new Tetrominoes[BoardWidth * BoardHeight];	//배열
       addKeyListener(new TAdapter());	//특정 메소드만을 이용하므로 리느서 대신 어댑터 이용
       clearBoard();  //보드를 깨끗하게
    }

    public void actionPerformed(ActionEvent e) {	//액션 발생 시 액션 구현
        if (isFallingFinished) {	//떨어져서 충돌이 일어났는가?
            isFallingFinished = false;	//충돌이 안일어났다.
            newPiece();	//새로운 블럭 생성
        } else {
            oneLineDown();	//그렇지 않으면 줄 삭제 후 점수 증가
        }
    }


    int squareWidth() { return (int) getSize().getWidth() / BoardWidth; }	//블록을 구성하는 사각형의 너비
    int squareHeight() { return (int) getSize().getHeight() / BoardHeight; }	//블록을 구성하는 사각형의 높이
    Tetrominoes shapeAt(int x, int y) { return board[(y * BoardWidth) + x]; } // 반복문을 두번 돌리지 않고 한번만으로 2차원 배열을 원소를 출력하기위해 식의 값을 사용 식의 값 설명이 필요


    public void start()	//시작 메소드와 변수들 다시 초기화
    {
        if (isPaused)	//예외 발생 시 리턴
            return;

        isStarted = true;
        isFallingFinished = false;
        numLinesRemoved = 0;
        clearBoard();
        gamestatus = false;

        newPiece();
        t.start();
        PlaySound("bgm.wav",true);
    }

    private void pause()	//잠시 정지
    {
        if (!isStarted)
            return;

        isPaused = !isPaused;	//정지했다면 정지 상태를 변화
        if (isPaused) {
            t.stop();
            score.setText("paused");
        } else {
            t.start();
            score.setText(String.valueOf(numLinesRemoved*100)); //라인이 줄어든 만큼 라벨에 표기
        }
        repaint();	//다시 그리기 paint를 직접 호출하지 않아야 한다. board가 이벤트를 처리하므로 board를 다시 그리게 한다.
    }

    public void paint(Graphics g)
    { 
        super.paint(g);

        Dimension size = getSize();
        int boardTop = 0;//(int) size.getHeight() - BoardHeight * squareHeight(); //시작 위치

        for (int i = 0; i < BoardHeight; ++i) {
            for (int j = 0; j < BoardWidth; ++j) {
                Tetrominoes shape = shapeAt(j, BoardHeight - i - 1);
                if (shape != Tetrominoes.NoShape)
                    drawSquare(g, 0 + j * squareWidth(),
                               boardTop + i * squareHeight(), shape);// 2, 3번 째 = 좌표, 그에 맞는 shape 블럭 가져옴. 
            }
         }

        if (curPiece.getShape() != Tetrominoes.NoShape) {  //현재 모양이 Tetrominoes의 NoShape가 아니라면
            for (int i = 0; i < 4; ++i) {
                int x = curX + curPiece.x(i);      //x의 좌표
                int y = curY - curPiece.y(i);    //y의 좌표
                drawSquare(g, 0 + x * squareWidth(),
                           boardTop + (BoardHeight - y - 1) * squareHeight(),
                     curPiece.getShape());           //drawSquare를 호출하여 도형을 그린다.
            }
        }
    }

    private void dropDown()
    {
        int newY = curY;           //현재의 Y를 newY 변수에 넣는다.
        while (newY > 0) {            //만약 newY가 0보다 크다면 while문을 수행하는데,
            if (!tryMove(curPiece, curX, newY - 1))   //만약 curX<0 || curX>=BoardWidth || newY-1<0 || newY-1>=BoardHeight 이면
             
                break;   //break를 하고
            --newY;   //new! Y를 감소시킨다.
        }
        pieceDropped();   //peiceDropped()를 호출한다.
    }

    private void oneLineDown()   //한 줄을 내리는 함수
    {
        if (!tryMove(curPiece, curX, curY - 1))  //만약 curX<0 || curX>=BoardWidth || curY-1<0 || curY-1>=BoardHeight 이고
         pieceDropped();          //shapeAt(curX,xurY-1)이 != Tetrominoes.NoShape이면
        //조각을 떨어뜨린다.
    }

    private void clearBoard()     //보드를 초기화하는 함수.
    {
        for (int i = 0; i < BoardHeight * BoardWidth; ++i)
            board[i] = Tetrominoes.NoShape;          //보드배열에 빈 공간을 넣는다.
    }

    private void pieceDropped()     //블록을 떨어뜨리는 함수
    {
        for (int i = 0; i < 4; ++i) {  //4개의 블록
            int x = curX + curPiece.x(i);     //블록 x의 좌표
            int y = curY - curPiece.y(i);    //블록 y의 좌표
            board[(y * BoardWidth) + x] = curPiece.getShape();  //보드의 배열에 블록을 넣는다.(2차원 배열)
        }

        removeFullLines(); //removeFullLines()를 호출

        if (!isFallingFinished)       //만약 블록 충돌이 일어나지 않았다면
            newPiece();            //newPiece()를 호출
    }

    private void newPiece()
    {
        curPiece.setRandomShape();      //현재 조각을 랜덤모양으로 생성
        curX = BoardWidth / 2 + 1;     
        curY = BoardHeight - 1 + curPiece.minY();

        if (!tryMove(curPiece, curX, curY)) {   //trymove가 false이면
//            curPiece.setShape(Tetrominoes.NoShape);   //현재 조각을 빈공간으로 바꾸고
            
            isStarted = false;
            
           JOptionPane op = new JOptionPane();
           op.showMessageDialog(null, "Game Over!");
           t.stop();
        }
        
    }

    private boolean tryMove(Shape newPiece, int newX, int newY) // 블럭 이동시키기
    {
        for (int i = 0; i < 4; ++i) {
            int x = newX + newPiece.x(i); // newpiece에 입력받은 x값을 더하기
            int y = newY - newPiece.y(i); // newpiece에 입력받은 y값을 더하기
            if (x < 0 || x >= BoardWidth || y < 0 || y >= BoardHeight) // x,y가 0보다 작고, 보드판을 넘어갔을때 false 리턴
                return false;
            if (shapeAt(x, y) != Tetrominoes.NoShape) // x,y의 좌표에 이미 블럭이 있을때 false를 리턴
                return false;
        }

        curPiece = newPiece; // newpiece의 값을 현재 조각에 넣기
        curX = newX;
        curY = newY;
        repaint();
        return true;
    }

    private void removeFullLines() // 줄이 꽉 차면 삭제
    {
        int numFullLines = 0; // 채워진 줄의 갯수

        for (int i = BoardHeight - 1; i >= 0; --i) {
            boolean lineIsFull = true; // 라인이 차있는 상태

            for (int j = 0; j < BoardWidth; ++j) {
                if (shapeAt(j, i) == Tetrominoes.NoShape) { //라인이 비었는지 확인
                    lineIsFull = false; // 라인이 비어있음
                    break;
                }
            }

            if (lineIsFull) { // 라인이 차있는걸 확인했을때
                ++numFullLines; // 채워진 라인 갯수 추가
                for (int k = i; k < BoardHeight - 1; ++k) {
                    for (int j = 0; j < BoardWidth; ++j)
                         board[(k * BoardWidth) + j] = shapeAt(j, k + 1);
                }
            }
        }

        if (numFullLines > 0) {
            numLinesRemoved += numFullLines; // 삭제된 라인 갯수 추가
            score.setText(String.valueOf(numLinesRemoved));//라인 삭제 메세지
            isFallingFinished = true; // 충돌 체크
            curPiece.setShape(Tetrominoes.NoShape);//현재 조각을 비운다
            repaint();
        }
     }

    private void drawSquare(Graphics g, int x, int y, Tetrominoes shape) // 상자 그리기
    {
        Color colors[] = { new Color(0, 0, 0), new Color(204, 102, 102), // 사용할 색상 지정
            new Color(102, 204, 102), new Color(102, 102, 204), 
            new Color(204, 204, 102), new Color(204, 102, 204), 
            new Color(102, 204, 204), new Color(218, 170, 0)
        };


        Color color = colors[shape.ordinal()];

        g.setColor(color); // 변수로 받아온 g에 색을 채우고, 선을 그림
        g.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2);

        g.setColor(color.brighter());
        g.drawLine(x, y + squareHeight() - 1, x, y);
        g.drawLine(x, y, x + squareWidth() - 1, y);

        g.setColor(color.darker());
        g.drawLine(x + 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + squareHeight() - 1);
        g.drawLine(x + squareWidth() - 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + 1);
    }

    class TAdapter extends KeyAdapter { //키 입력을 받는 클래스
         public void keyPressed(KeyEvent e) { 

             if (!isStarted || curPiece.getShape() == Tetrominoes.NoShape) {  // 게임을 시작하지 않았거나, 현재 조각이 없을 경우
                 return;
             }

             int keycode = e.getKeyCode(); // e에서 입력받은 키를 추출

             if (keycode == 'p' || keycode == 'P') { // p를 입력하면 pause
                 pause();
                 return;
             }

             if (isPaused) // 이미 퍼즈인 경우
                 return;

             switch (keycode) {
             case KeyEvent.VK_LEFT:
                 tryMove(curPiece, curX - 1, curY);
                 break;
             case KeyEvent.VK_RIGHT:
                 tryMove(curPiece, curX + 1, curY);
                 break;
             case KeyEvent.VK_DOWN:
                 tryMove(curPiece, curX, curY-1);
                 break;
             case KeyEvent.VK_UP:
                 tryMove(curPiece.rotateLeft(), curX, curY);
                 break;
             case KeyEvent.VK_ENTER:
                 tryMove(curPiece, curX, curY+1);
                 break;    
             case KeyEvent.VK_SPACE:
                 dropDown();
                 break;

             }

         }
     }
}
    
    Clip clip;

    public void PlaySound(String file,boolean Loop)
    {
    	    
    	   try{
    		   AudioInputStream ais = AudioSystem.getAudioInputStream(new BufferedInputStream(new FileInputStream(file)));
    		   clip = AudioSystem.getClip();
    		   clip.open(ais);
    		   clip.start();
    		   
    		   if(Loop ==true) clip.loop(-1);
    		   
    		  
    	   }
    	   catch(Exception e){
    		   e.printStackTrace();
    	   }
    }
}
