package Tetris;


import java.awt.BorderLayout;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Tetris extends JFrame {

    JLabel score;


    public Tetris() {

        score = new JLabel(" 0");
        add(score, BorderLayout.SOUTH);
        Board board = new Board(this);
        add(board);
        board.start();

        setSize(300, 600);
        setTitle("Tetris");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
   }

   public JLabel getscore() {
       return score;
   }

    public static void main(String[] args) {

        Tetris game = new Tetris();
       
        game.setLocationRelativeTo(null);
        game.setVisible(true);

    }
} 

 

 